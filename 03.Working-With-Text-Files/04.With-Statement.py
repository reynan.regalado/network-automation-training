with open('configuration.txt') as file:
    content = file.read()
    print(content)

    # file is open inside of code block (False)
    print(file.closed)

#file is closed outside of code block (True)
print(file.closed)
