#back slash causes errors
# f = open('C:\PyCharm\Udemy\Python-For-Network-Engineers\pythonProject\03.Working-With-Text-Files\settings.txt')

#use double backslash
# f = open('C:\\PyCharm\\Udemy\\Python-For-Network-Engineers\\pythonProject\\03.Working-With-Text-Files\\settings.txt')

#use raw strings which treats backslash as literal characters
# f = open(r'C:\PyCharm\Udemy\Python-For-Network-Engineers\pythonProject\03.Working-With-Text-Files\settings.txt')

# . (single dot represents current working directory)
# .. (double dot represents parent working directory)

#relative path
f = open('settings.txt')