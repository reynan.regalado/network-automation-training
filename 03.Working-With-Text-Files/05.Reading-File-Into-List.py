#ways to read files into list.

#1: f.read().splitlines()
with open ('configuration.txt') as f:
    content = f.read().splitlines()
    print(content)

print('\n', '#'  * 50, '\n')

#2: f.readlines() # readlines reads all lines
with open('configuration.txt') as f:
    content = f.readlines()
    print(content)

print('\n', '#' * 50, '\n')

#3: f.readline() # readline reads single line and moves the cursor
with open('configuration.txt') as f:
    print(f.readline())
    print(f.readline())


print('\n', '#' * 50, '\n')

#4: list(f) where f is the file object
with open('configuration.txt') as f:
    content = list(f)
    print(content)

print('\n', '#' * 50, '\n')

#iterate over a file
with open('configuration.txt') as f:
    for line in f:
        print(line, end='')