#rt is read-text mode
#rb is read-binary mode

f = open('configuration.txt', 'rt')
content = f.read()
print(content)

#closed attribute check if closed = false, or closed = true
print('\n#####\n')
print('Is the file closed?', f.closed)

#close the file
f.close()

print('\n#####\n')
print('Is the file closed?', f.closed)
