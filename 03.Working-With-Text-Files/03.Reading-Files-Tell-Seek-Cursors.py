f = open('configuration.txt')
content = f.read()
print(content)

print('\n#######\n')

#print first 5 characters
f = open('configuration.txt')
content = f.read(5)
print(content)

print('\n#######\n')

#print next 3 characters
content = f.read(3)
print(content)

print('\n#######\n')

#print cursor position
print('Cursor position is', f.tell())

print('\n#######\n')

#seek positions the cursor, starts from beginning
#start at position 2 and read next 3
f.seek(2)
content = f.read(3)
print(content)