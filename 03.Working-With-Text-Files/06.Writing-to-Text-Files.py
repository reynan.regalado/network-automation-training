# 'w' will create new file or overrite if file exist.
with open('myfile.txt', 'w') as f:
    f.write('Just a line.\n')
    f.write('Just a 2nd line.\n')

# 'a' will create new file or append if file exist.
with open('myfile.txt', 'a') as f:
    f.write('Just a 3rd line.\n')
    f.write('Just a 4th line.')

# 'r+' file must exist. adds new content at beginning of the file. can read/write.
with open('myfile.txt', 'r+') as f:
    f.write('Line added with r+\n')
    f.seek(10)
    f.write('cursor seek test.')
    f.seek(10)
    print(f.read())